package com.sonatasmx.googleextendedtj;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sonatamx on 5/27/2015.
 */
public class EventDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "extendedtj"; // the name of our database
    private static final int DB_VERSION = 4;

    EventDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS EVENT");
        db.execSQL("CREATE TABLE EVENT ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "TITLE TEXT, "
                + "DESCRIPTION TEXT, "
                + "IMAGE_RESOURCE_ID INTEGER,"
                + "DATE TEXT, "
                + "START_HOUR TEXT, "
                + "END_HOUR TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}