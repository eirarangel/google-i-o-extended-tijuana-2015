package com.sonatasmx.googleextendedtj;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ScheduleFragment extends Fragment {

    public static final String TAG = ScheduleFragment.class.getSimpleName();
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    public static ScheduleFragment newInstance() {
        return new ScheduleFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tabbed, container, false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getChildFragmentManager());

        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        return v;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TabbedContentFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }
    }

    public static class TabbedContentFragment extends Fragment {

        public static final String ARG_SECTION_NUMBER = "section_number";
        int mNum;
        SQLiteOpenHelper eventsDatabaseHelper;
        SQLiteDatabase db;

        public TabbedContentFragment() {
        }

        static TabbedContentFragment newInstance(int num) {
            TabbedContentFragment f = new TabbedContentFragment();

            Bundle args = new Bundle();
            args.putInt("num", num + 1);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabbed_content,
                    container, false);
            RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.cardList);
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);

            EventAdapter eventAdapter = new EventAdapter(createList(mNum));
            recList.setAdapter(eventAdapter);
            return rootView;
        }

        private List<Event> createList(int section) {
            List<Event> result = new ArrayList<Event>();
            eventsDatabaseHelper = new EventDatabaseHelper(getActivity());
            db = eventsDatabaseHelper.getReadableDatabase();
            String selection = "2015-05-28";
            String dateEvent = "28 Mayo";
            if(section == 2)
            {
                selection = "2015-05-29";
                dateEvent = "29 Mayo";
            }
            Cursor cursor = db.query("EVENT",
                    new String[] { "TITLE", "DATE", "START_HOUR", "END_HOUR"},
                    "DATE = ?", new String[] {selection}, null, null, null);
            while (cursor.moveToNext()) {
                Event ev = new Event();
                ev.title = cursor.getString(0);
                ev.date = dateEvent + " - " + cursor.getString(2) + " / " + cursor.getString(3);
                result.add(ev);
            }
            cursor.close();
            db.close();
            return result;
        }
    }
}
